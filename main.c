#include "BearLibTerminal.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

#include <windows.h>

const int WINDOW_HEIGHT = 40;
const int WINDOW_WIDTH = 100;
const int MAX_STEPS = 50;

struct Vec2
{
    float x;
    float y;
};

struct Vec3
{
    float x;
    float y;
    float z;
};

float vec2_norm(struct Vec2 p)
{
    return sqrt(p.x * p.x + p.y * p.y);
}

float vec3_norm(struct Vec3 p)
{
    return sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
}

struct Vec3 vec3_normalized(struct Vec3 v)
{
    float norm = vec3_norm(v);
    struct Vec3 normalized = {.x = v.x / norm, .y = v.y / norm, .z = v.z / norm};
    return normalized;
}

struct Vec3 vec3_add(struct Vec3 a, struct Vec3 b)
{
    struct Vec3 sum = {.x = a.x + b.x, .y = a.y + b.y, .z = a.z + b.z};
    return sum;
}

struct Vec3 vec3_sub(struct Vec3 a, struct Vec3 b)
{
    struct Vec3 sum = {.x = a.x - b.x, .y = a.y - b.y, .z = a.z - b.z};
    return sum;
}

struct Vec3 vec3_rotate_x(struct Vec3 v, float angle)
{
    struct Vec3 temp;
    temp.x = v.x;
    temp.y = v.y * cos(angle) + v.z * sin(angle);
    temp.z = -v.y * sin(angle) + v.z * cos(angle);
    return temp;
}

struct Vec3 vec3_rotate_y(struct Vec3 v, float angle)
{
    struct Vec3 temp;
    temp.x = v.x * cos(angle) + v.z * sin(angle);
    temp.z = -v.x * sin(angle) + v.z * cos(angle);
    temp.y = v.y;
    return temp;
}

struct Vec3 vec3_rotate_z(struct Vec3 v, float angle)
{
    struct Vec3 temp;
    temp.x = v.x * cos(angle) + v.y * sin(angle);
    temp.y = -v.x * sin(angle) + v.y * sin(angle);
    temp.z = v.z;
    return temp;
}

struct Vec3 vec3_mul(struct Vec3 v, float a)
{
    v.x *= a;
    v.y *= a;
    v.z *= a;
    return v;
}

int main()
{
    terminal_open();

    char *setting_string = malloc(200 * sizeof(char));
    sprintf(setting_string, "font: UbuntuMono-R.ttf, size=10; \nwindow.title=\"Render Demo\"; window.size=%ix%i;input.filter=[keyboard, mouse]; input.mouse-cursor=false; window.fullscreen=true;", WINDOW_WIDTH, WINDOW_HEIGHT);
    printf("Created terminal with size %ix%i\n", WINDOW_WIDTH, WINDOW_HEIGHT);
    terminal_set(setting_string);
    free(setting_string);

    terminal_refresh();

    int input;

    int frame_number = 0;

    struct Vec3 spheres[10] = {0};

    for (int i = 0; i < 10; i++)
    {
        spheres[i].x = (float)(rand() % 2000) / 100 - 10;
        // spheres[i].y = (float)(rand()%2000)/100;
        spheres[i].z = (float)(rand() % 2000) / 100 - 10;
    }
    float radius = 1.f;

    struct Vec3 camera_pos = {.x = 0, .y = 1.0, .z = -1.0f};
    float rotation_angle = 0;
    float rotation_vert = 0;

    float fudge_factor = (float)terminal_state(TK_CELL_HEIGHT) / terminal_state(TK_CELL_WIDTH);
    float fudge_multiplier = (float)terminal_state(TK_HEIGHT) / terminal_state(TK_WIDTH);

    int XMAX = terminal_state(TK_CELL_WIDTH) * terminal_state(TK_WIDTH);
    int YMAX = terminal_state(TK_CELL_HEIGHT) * terminal_state(TK_HEIGHT);

    POINT mousepos;

    bool is_fullscreen = true;

    printf("Entering main loop...\n");
    while (input != TK_CLOSE)
    {
        GetCursorPos(&mousepos);
        terminal_clear();
        rotation_angle = 2 * (float)mousepos.x / XMAX - 1.0;
        rotation_angle *= 3 * M_2_PI;

        rotation_vert = -0.8 * (2 * ((float)mousepos.y / YMAX - 1.0));
        rotation_vert *= M_PI_4;

        if (frame_number++ % 10 == 0)
        {
            for (int i = 0; i < 10; i++)
            {
                spheres[i].x = (float)(rand() % 2000) / 100 - 10;
                // spheres[i].y = (float)(rand()%2000)/100;
                spheres[i].z = (float)(rand() % 2000) / 100 - 10;
            }
        }

        for (size_t screenX = 0; screenX < WINDOW_WIDTH; screenX++)
        {
            for (size_t screenY = 0; screenY < WINDOW_HEIGHT; screenY++)
            {
                // CAST RAY
                struct Vec3 ray_dir = {
                    .x = (2 * (float)screenX / (float)WINDOW_WIDTH - 1.0),
                    .y = -(fudge_multiplier * fudge_factor) * (2 * (float)screenY / (float)WINDOW_HEIGHT - 1.0),
                    .z = 1};

                ray_dir = vec3_normalized(ray_dir);
                ray_dir = vec3_mul(ray_dir, 0.2);

                ray_dir = vec3_rotate_x(ray_dir, rotation_vert);
                ray_dir = vec3_rotate_y(ray_dir, rotation_angle);

                struct Vec3 pos = camera_pos;

                for (size_t step_nr = 0; step_nr < MAX_STEPS; step_nr++)
                {
                    color_t col;

                    for (size_t i = 0; (i < 10) && (pos.y > 0); i++)
                    {
                        if (spheres[i].x != 0.0 || spheres[i].y != 0.0 || spheres[i].z != 0.0)
                        {
                            // printf("found sphere %i", i);
                            float dist = vec3_norm(vec3_sub(pos, spheres[i]));

                            if (dist < radius)
                            {
                                // HIT!
                                col = color_from_argb(200 - fmax(-200, fmin(255, (255 * pow(vec3_norm(vec3_sub(pos, camera_pos)), 0.75) / 10))), 255, 255, 255);
                                terminal_color(col);

                                terminal_printf(screenX, screenY, "▓");
                                goto check_controls;
                            }
                        }
                    }

                    struct Vec3 Zero3 = {0.0};
                    if (pos.y < 0)
                    {
                        // HIT FLOOR
                        // col = color_from_argb(255 - (uint8_t)(vec3_norm(pos)/(2*MAX_STEPS)), 0, 255, 255);
                        col = color_from_argb(255 - (uint8_t)fmin(255, 30 * pow(vec3_norm(vec3_sub(pos, camera_pos)), 0.9)), 0, 255, 255);
                        terminal_color(col);
                        terminal_print(screenX, screenY, "#");
                        break;
                    }
                    pos = vec3_add(pos, ray_dir);

                check_controls:;
                }
            }
        }
        // sun_bloom_radius = rand()%10/50.0;
        terminal_refresh();

        while (terminal_has_input())
        {
            input = terminal_read();
            struct Vec3 walk_dir = {.x = 0.0, .y = 0.0, .z = 0.1};
            walk_dir = vec3_rotate_y(walk_dir, rotation_angle);
            walk_dir = vec3_mul(walk_dir, 1.75);
            switch (input)
            {
            case TK_W:
                camera_pos = vec3_add(camera_pos, walk_dir);
                break;
            case TK_A:
                camera_pos = vec3_add(camera_pos, vec3_rotate_y(walk_dir, -M_PI_2));
                break;
            case TK_S:
                camera_pos = vec3_sub(camera_pos, walk_dir);
                break;
            case TK_D:
                camera_pos = vec3_add(camera_pos, vec3_rotate_y(walk_dir, M_PI_2));
                break;

            case TK_F:
                is_fullscreen = !is_fullscreen;
                if (is_fullscreen)
                {
                    terminal_set("window.fullscreen=true");
                }
                else
                {
                    terminal_set("window.fullscreen=false");
                }

            case TK_LEFT:
                camera_pos.x -= 0.1;
                break;
            case TK_RIGHT:
                camera_pos.x += 0.1;
                break;
            case TK_UP:
                camera_pos.z += 0.1;
                break;
            case TK_DOWN:
                camera_pos.z -= 0.1;
                break;
            case TK_KP_PLUS:
                camera_pos.y += 0.1;
                break;
            case TK_KP_MINUS:
                camera_pos.y -= 0.1;
                break;
            case TK_E:
                rotation_angle += M_PI / 20;
                break;
            case TK_Q:
                rotation_angle -= M_PI / 20;
                break;
            case TK_ESCAPE:
                return 0; // CRASH AND BURN
            default:
                break;
            }
        }
    }

    terminal_close();
    return 0;
}